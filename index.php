<?php
/*
@package assaftheme
*/
?>

<?php
if (have_posts()):
	while (have_posts()):
		the_post();
    get_header();

		get_template_part( 'template-parts/contents/content');

	endwhile;
endif;
wp_reset_query()
?>
