<?php
/**
 * This is the template for the footer
 * @package assaf
 */
?>
</div> <!--container-fluid-->

<div class="container-fluid">
    <?php
        get_template_part( 'template-parts/footers/footer', 'page' );
    ?>
</div>

<?php wp_footer(); ?>

</body>
</html>
