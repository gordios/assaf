<!-- Wordpress dynamic menu -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #f9fafb;">
  <a class="navbar-brand" href="#">ASSAF</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor03">

    <?php  $menu = new Assaf()?>
    <?=  $menu->generate_header_menu()?>

    <form class="form-inline">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Go</button>
    </form>
  </div>
</nav>
