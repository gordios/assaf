<header class="text-center mb-5">
    <!-- main header -->
    <?php get_template_part('template-parts/headers/header-parts/header', 'main') ?>
    <!-- header-bottom -->
    <?php get_template_part('template-parts/headers/header-parts/menu') ?>

</header> <!-- header -->
