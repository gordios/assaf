<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <?php if(has_post_thumbnail()) { ?>
                <section class="my-4 text-center">
                    <img src="<?=the_post_thumbnail_url()?>" class="img-fluid" alt="">
                </section>
            <?php }?>

            <section class="mb-5">
                <h2 class="columnStyle text-center pb-2"><?=the_title()?></h2>
                <p class="move-from-bottom delay-400 text-center">
                    <?=the_content()?>
                </p>
            </section>
        </div>
    </div>
</div>