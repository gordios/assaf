<section id="body" class="assaf-background-color py-2 px-2 text-md-left text-sm-center mb-5">
  <div class="row">
    <!-- Logo column start-->
    <div class="col-md-3">
        <?php

        $args0 = array(
            'posts_per_page'     => 7,
            'order'           => 'DESC',
            'post_type'       => 'image',
            'post_status'     => 'publish',
        );
        $loop0 = new WP_Query($args0);

        if ($loop0->have_posts()):
            while ($loop0->have_posts()):
                $loop0->the_post();

                get_template_part( 'template-parts/contents/post_types/content', 'image' );

            endwhile;
        endif;
        wp_reset_query()
        ?>
    </div>
    <!-- Logo column end-->

    <!-- News column start-->
    <div class="col-md-9">

        <h1 class="title-color bold mb-5">ASSAf News</h1>

        <?php

        $args = array(
            'posts_per_page'     => 5,
            'order'           => 'DESC',
            'post_type'       => 'news',
            'post_status'     => 'publish',
        );
        $loop = new WP_Query($args);

        if ($loop->have_posts()):
            while ($loop->have_posts()):
                $loop->the_post();

                get_template_part( 'template-parts/contents/post_types/content', 'news' );

            endwhile;
        endif;
        wp_reset_query()
        ?>
    </div>
    <!-- News column end-->
  </div>
</section>
