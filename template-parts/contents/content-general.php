<section id="general" class="assaf-background-color py-2 px-2 text-md-left text-sm-center mb-5">
  <div class="row">
    <div class="col-md-12">
      <h1 class="title-color bold">General News</h1>
    </div>

      <?php

      $args = array(
          'posts_per_page'     => 2,
          'order'           => 'DESC',
          'post_type'       => 'general',
          'post_status'     => 'publish',
      );
      $loop = new WP_Query($args);

      if ($loop->have_posts()):
          while ($loop->have_posts()):
              $loop->the_post();

              get_template_part( 'template-parts/contents/post_types/content','general');

          endwhile;
      endif;
      wp_reset_query()
      ?>

  </div>
</section>
