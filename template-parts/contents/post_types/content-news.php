<div class="row">
  <div class="col-md-12">
    <h5> <a class="subtitle-color" target="_blank" href="<?php the_permalink() ?>"><?php the_title() ?></a> </h5>
    <p><small><?php the_time('l, j F Y'); ?></small></p>
    <div class="description-bloc py-2 px-2 mb-4">
      <div class="row">
        <div class="col-md-4">
          <img src="<?= the_post_thumbnail_url() ?>"  class="img-thumbnail">
        </div>
        <div class="col-md-8">
          <p>
              <?php the_excerpt() ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

