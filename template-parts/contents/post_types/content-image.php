<div class="row">
    <div class="col-md-12 mb-4">
        <a href="<?php the_permalink()?>" target="_blank">
            <img src="<?= the_post_thumbnail_url() ?>"  class="img-thumbnail">
        </a>
    </div>
</div>