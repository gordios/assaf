<div class="col-md-6">
    <h5 class="subtitle-color">
        <a target="_blank" class="title-color" href="<?php the_permalink() ?>"><?php the_title() ?></a>
    </h5>
    <p>
        <?php the_excerpt() ?>
    </p>
</div>