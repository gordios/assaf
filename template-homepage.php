<?php
/*
@package assaf

Template Name: Homepage
*/

//CUSTOM THEME POST START

if( have_posts()):
  while( have_posts() ): the_post();

    get_header();

    $template_parts = ['general', 'body'];

    foreach ($template_parts as $template_part) {
      get_template_part( 'template-parts/contents/content', $template_part );
    }

    get_footer();

  endwhile;
endif;
wp_reset_query();
//CUSTOM THEME POST END

?>
