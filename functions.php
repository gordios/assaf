<?php
//css and jss loading
require get_template_directory() .'/inc/enqueue.php';
//walker class
require get_template_directory() .'/inc/walker.php';
//theme support
require get_template_directory() .'/inc/theme-support.php';
//function front-end
require get_template_directory() .'/inc/function-frontend.php';
//function post types
require get_template_directory() .'/inc/custom_post_type.php';
//function post types
require get_template_directory() .'/inc/assaf-menu.php';


//Disable plugin and theme auto-update
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );
