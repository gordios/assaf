<?php
/*
@package assaf
===========================
front-end enqueue functions
===========================
*/
function assaf_load_scripts() {
    //bootstrap 4.0.0 files
    wp_enqueue_style( 'bootstrap', get_template_directory_uri(). '/css/bootstrap.min.css', array(), '4.0.0', 'all' );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri(). '/js/bootstrap.min.js', array(), '4.0.0', true );

    //Add assaf css file in CSS folder generate by sass.
    wp_enqueue_style( 'assaf', get_template_directory_uri(). '/css/assaf.css', array(), '0.0.5', 'all' );
    wp_enqueue_script( 'assaf', get_template_directory_uri(). '/js/assaf.js', array('jquery'), '0.0.1', true );

    //font-awesome css
    wp_enqueue_style( 'font-awesome', get_template_directory_uri(). '/css/font-awesome.min.css', array(), '4.0.0', 'all' );

}
add_action( 'wp_enqueue_scripts', 'assaf_load_scripts');


function assaf_load_admin_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( 'toolset', get_template_directory_uri(). '/inc/admin-script/admin.js', array('jquery'), '1.0.4', true );
}
add_action( 'admin_enqueue_scripts', 'assaf_load_admin_scripts');

?>
