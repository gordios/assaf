<?php


class Assaf
{
    protected $locations;

    protected $header_menu;

    protected $header_menu_items;

    protected $footer_menu;

    protected $footer_menu_items;


    public function  __construct()
    {

        $this->locations = get_nav_menu_locations();

        $this->header_menu = wp_get_nav_menu_object( $this->locations[ 'primary' ] );

        $this->header_menu_items = wp_get_nav_menu_items($this->header_menu->term_id); //can take menu_name as parameter, 'home_menu'

        $this->footer_menu = wp_get_nav_menu_object( $this->locations[ 'secondary' ] );

        $this->footer_menu_items = wp_get_nav_menu_items($this->footer_menu->term_id);
    }

    /*
    * =======================
    *   ASSAF HEADER MENU
    * =======================
    * */

    public function generate_header_menu() {
        $menu_parents = $this->get_parent_menus($this->header_menu_items);
        $line = '<ul class="navbar-nav mr-auto">';

        foreach($menu_parents as $menu_parent) {
            $child_menus = $this->get_child_menus_of($menu_parent, $this->header_menu_items);

            if(count($child_menus) == 0) {
                $line .= $this->get_item_content_of($menu_parent);

            } elseif (count($child_menus) >= 1) {
                $line .= $this->get_child_item_content_of($menu_parent, $child_menus);
            }
        }

        $line.='</ul>';

        return $line;
    }

    public function get_child_item_content_of($menu, $child_menus) {
        $line_content = '';

        $line_content .= $this->get_dropdown_content($child_menus);

        return '<li class="nav-item dropdown">'
            .$this->get_link_content_of($menu, true)
            .'<div class="dropdown-menu" aria-labelledby="'.$menu->ID.'">'
                .$line_content.
            '</div>
            </li>';
    }

    public function get_link_content_of($menu, $child_parent = false, $dropdown_item = false) {
        if($child_parent && !$dropdown_item) {
            $class_names = 'nav-link dropdown-toggle';
        } elseif (!$child_parent && !$dropdown_item) {
            $class_names = 'nav-link '.$this->get_menu_classes($menu);
        } elseif(!$child_parent && $dropdown_item) {
            $class_names = 'dropdown-item '.$this->get_menu_classes($menu);
        }

        $a = '<a 
            class="'.$class_names.'"'
            .$this->get_link_attribute($menu, true).
            'href="'.$menu->url.'">'
            .$menu->title.
            '</a>';

        return $a;
    }

    public function get_link_attribute($menu, $child_parent = false) {
        $attributes = ! empty( $menu->attr_title ) ? ' title="' . esc_attr($menu->attr_title) . '"' : '';
        $attributes .= ! empty( $menu->target ) ? ' target="' . esc_attr($menu->target) . '"' : '';
        $attributes .= ! empty( $menu->xfn ) ? ' rel="' . esc_attr($menu->xfn) . '"' : '';
        $attributes .= ! empty( $menu->url ) ? ' href="' . esc_attr($menu->url) . '"' : '';

        if($child_parent) {
            $attributes .='id="'.$menu->ID.'" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"';
        }

        return $attributes;
    }

    public function get_dropdown_content($child_menus) {
        $line = '';
        foreach ($child_menus as $child_menu) {
            $line .= $this->get_link_content_of($child_menu, false, true);
        }

        return $line;
    }

    public function get_parent_menus($menu_items) {
        if($menu_items) {
            $menu_parents = [];
            foreach ($menu_items as $item) {
                if($item->menu_item_parent == '0') {
                    array_push($menu_parents, $item);
                }
            }
            return $menu_parents;
        }
    }

    public function get_item_content_of($menu) {
        return '<li class="nav-item">'.$this->get_link_content_of($menu).'</li>';
    }

    public function get_menu_classes($menu) {
        $classes = '';

        foreach ($menu->classes as $class_name) {
            $classes .= $class_name.' ';
        }

        return $classes;
    }

    public function get_child_menus_of($menu, $location_menus) {
        $child_menus = [];

        foreach ($location_menus as $menu_item) {
            if ($menu_item->menu_item_parent == $menu->ID) {
                array_push($child_menus, $menu_item);
            }
        }
        return $child_menus;
    }

    /*
    * =======================
    *   ASSAF FOOTER MENU
    * =======================
    * */

    public function generate_footer_menu() {
        $menu_parents = $this->get_parent_menus($this->footer_menu_items);

        $line = '<div class="row">';

        foreach($menu_parents as $menu_parent) {
            $child_menus = $this->get_child_menus_of($menu_parent, $this->footer_menu_items);

            if(count($child_menus) == 0) {
                $line .= $this->get_footer_item_content_of($menu_parent);

            } elseif (count($child_menus) > 0) {

                $line .= $this->get_footer_child_item_content_of($menu_parent, $child_menus);
            }
        }

        $line.='</div>';

        return $line;
    }

    public function get_footer_item_content_of($menu) {
        return '<div class="col-md-3">
                    <h4>'.$menu->title.'</h4>
                </div>';
    }

    public function get_footer_child_item_content_of($menu_parent, $child_menus) {
        $li_line_content = '';
        foreach ($child_menus as $child_menu) {
            $li_line_content .= '<li>'
                                    .$child_menu->title.
                                '</li>';
        }

        return '<div class="col-md-3">
                    <h4>'.$menu_parent->title.'</h4>
                    <ul class="list-group">'
                        .$li_line_content.
                    '</ul>
                </div>';
    }

}