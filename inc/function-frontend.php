<?php
  function get_assaf_meta_field($meta_field_ID) {
    $postID = get_the_id();
    return get_post_meta( $postID, $meta_field_ID, true );
  }
?>
