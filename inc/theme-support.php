 <?php
/**
* Add theme support option
* @package assaf
*/

/*
======================
Activate Nav Menu Option
======================
 */
function assaf_register_nav_menu(){
    add_theme_support( 'menus' );
    register_nav_menu('primary', 'Header Navigation Menu');
    register_nav_menu('secondary', 'Footer Navigation Menu');
}
add_action( 'init', 'assaf_register_nav_menu');

/*
======================
Custom logo admin setup
======================
 */
function assaf_logo_setup() {
  $defaults = array(
      'height'      => 100,
      'width'       => 400,
      'flex-height' => false,
      'flex-width'  => false,
  );
  add_theme_support( 'custom-logo', $defaults );
}
add_action( 'init', 'assaf_logo_setup');

/*
======================
Custom widget setup
======================
 */
function assaf_widget_init() {
  register_sidebars(
    array(
      'name' => esc_html( 'assaf widget', 'assaftheme' ),
      'id' => 'assaf-widget',
      'description' => 'dynamic header widget',
      'before_widget' => '<section id="%1$s" class="assaf-widget" %2$s>',
      'after_widget' => '</section>',
      'before_title' => '<h2 class="assaf-widget-title"/>',
      'after_title' => '</h2>'
    )
  );
}
add_action( 'widgets_init', 'assaf_widget_init' );

 function custom_excerpt_length( $length ) {
     return 30;
 }
 add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//add_theme_support( 'custom-header' );
add_theme_support( 'post-thumbnails' );

?>
