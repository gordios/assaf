<?php

//POST TYPE GENERAL
function general_custom_type(){

    $labels = array(
        'name' => 'Assaf general',
        'singular_name' => 'General',
        'add_new' => 'Ajouter article general',
        'all_items' => 'Tous les articles generaux',
        'add_new_item' => 'Ajouter article general',
        'edit_item' => 'Modifier article general',
        'new_item' => 'Nouvel article general',
        'view_item' => 'Afficher article general',
        'search_item' => 'Chercher article general',
        'not_found' => 'Aucun article general trouvé',
        'not_found_in_trash' => 'Aucun article general trouvé dans la corbeille',
        'parent_item_colon' => 'Parent Item',
    );
    $args = array(

        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
        ),
        'menu_position' => 5,
        'exclude_from_search' => false,
    );
    register_post_type('general',$args);
}

add_action('init', 'general_custom_type');

//POST TYPE NEWS

function news_custom_type(){

    $labels = array(
        'name' => 'Assaf news',
        'singular_name' => 'News',
        'add_new' => 'Ajouter article news',
        'all_items' => 'Tous les articles news',
        'add_new_item' => 'Ajouter article news',
        'edit_item' => 'Modifier article news',
        'new_item' => 'Nouvel article news',
        'view_item' => 'Afficher article news',
        'search_item' => 'Chercher article news',
        'not_found' => 'Aucun article news trouvé',
        'not_found_in_trash' => 'Aucun article news trouvé dans la corbeille',
        'parent_item_colon' => 'Parent Item',
    );
    $args = array(

        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
        ),
        'menu_position' => 5,
        'exclude_from_search' => false,
    );
    register_post_type('news',$args);
}

add_action('init', 'news_custom_type');


//POST TYPE IMAGE

function image_custom_type(){

    $labels = array(
        'name' => 'Image',
        'singular_name' => 'Image',
        'add_new' => 'Ajouter article image',
        'all_items' => 'Tous les articles image',
        'add_new_item' => 'Ajouter article image (lien)',
        'edit_item' => 'Modifier article image',
        'new_item' => 'Nouvel article image',
        'view_item' => 'Afficher article image',
        'search_item' => 'Chercher article image',
        'not_found' => 'Aucun article image trouvé',
        'not_found_in_trash' => 'Aucun article image trouvé dans la corbeille',
        'parent_item_colon' => 'Parent Item',
    );
    $args = array(

        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
        ),
        'menu_position' => 5,
        'exclude_from_search' => false,
    );
    register_post_type('image',$args);
}

add_action('init', 'image_custom_type');